/***************************************************************************
*   Copyright (C) 2016 by Riccardo Redaelli, Fabiano Riccardi,            *
*   Andrea Salmoiraghi, Andrea Turri                                      *
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*   This program is distributed in the hope that it will be useful,       *
*   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
*   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
*   GNU General Public License for more details.                          *
*                                                                         *
*   As a special exception, if other files instantiate templates or use   *
*   macros or inline functions from this file, or you compile this file   *
*   and link it with other works to produce a work based on this file,    *
*   this file does not by itself cause the resulting work to be covered   *
*   by the GNU General Public License. However the source code for this   *
*   file must still be made available in accordance with the GNU General  *
*   Public License. This exception does not invalidate any other reasons  *
*   why a work based on this file might be covered by the GNU General     *
*   Public License.                                                       *
*                                                                         *
*   You should have received a copy of the GNU General Public License     *
*   along with this program; if not, see <http://www.gnu.org/licenses/>   *
***************************************************************************/

#include <iostream>
#include "chat.h"
#include "bluetooth.h"
#include "button/button.h"
#include "audio/adpcm_player.h"
#include "audio/bell.h"

using namespace std;
using namespace miosix;

#ifdef DEBUG
#define debug(str) do { std::cout << "[CHAT] " << str << std::endl; } while( false )
#else
#define debug(str) do { } while ( false )
#endif

//INITIALIZATION LIST
Chat::Chat(): bt(Bluetooth::instance()), led(Led::instance()), chatting(false), active(false) {}
//BTPHONE protocol
const string Chat::BTPHONE_PROTOCOL = "btPhone:";
const string Chat::BTPHONE_RING = "btPhone:RING";
const string Chat::BTPHONE_QUIT = "btPhone:QUIT";
const string Chat::BTPHONE_DISCONNECT = "btPhone:DISCONNECT";

Chat& Chat::instance()
{
        static Chat singleton;
        return singleton;
}

void Chat::init(){
        debug("Initializing...");
        if(!bt.isConnected()) {
                bt.init("/dev/auxtty", bind(&Chat::receive,this,placeholders::_1));
                bt.connect();
        }
        led.turnOn(kLedOrange);
        chatting = false;
        active = true;
        debug("Chat initialized!");
}

bool Chat::isActive(){
        return active;
}

bool Chat::isChatting(){
        return chatting;
}

void Chat::start() {
        debug("Start the chat...");
        active = true;
        chatting=false;
        //Thread that wait button to start the chat
        prepareToStart();
        //active=true;
        led.turnOff(kLedOrange);
        led.turnOn(kLedGreen);
        //Thread that wait button to quit the chat
        prepareToQuit();
}
/* [ENTER CHAT] */
void Chat::prepareToStart(){
        debug("Creating thread waiting button to connect...");
        Thread::create(&Chat::doEnter,2048,1,(void*)this,Thread::JOINABLE);
        debug("done");
        debug("Waiting button to enter in chat...");
        Lock<FastMutex> l(chatMutex);
        while(!chatting) {
                chatCV.wait(l);
        }
        debug("Entered in a chat!");
}
void* Chat::doEnter(void* arg){
        reinterpret_cast<Chat*>(arg)->enter();
        return (void*)0;
}
//Wait the button
//Send other device the command to play the bell audio
//Set chat active
void Chat::enter(){
        waitForButton();
        debug("Button pressed: enter the chat");
        if(chatting) {
                debug("Already in a chat!");
                return;
        }
        led.startBlink(kLedGreen);
        ringBell();
        bt.sendString(BTPHONE_RING);
        chatting=true;
        chatCV.broadcast();
}

/* [QUIT CHAT]
 *
 *  Initialize the thread waiting for the button pression to execute the quit
 */
void Chat::prepareToQuit(){
        debug("Creating thread waiting button to quit...");
        Thread::create(&Chat::doQuit,2048,1,(void*)this,Thread::JOINABLE);
        debug("done");
}
void* Chat::doQuit(void* arg){
        reinterpret_cast<Chat*>(arg)->quit();
        return (void*)0;
}
//Wait the button
//Send other device the command to stop
//Set chat not active
void Chat::quit(){
        waitForButton();
        debug("Button pressed: quit the chat");
        if(!chatting) {
                debug("Not in a chat!");
                return;
        }
        //Send other device command to quit the chat
        bt.sendString(BTPHONE_QUIT);
        chatOff();
}
void Chat::chatOff() {
        led.turnOff(kLedGreen);
        chatting = false;
        debug("Chat quitted");
}

void Chat::terminate() {
        chatOff();
        active=false;
        debug("Chat terminated... Bye!");
        bt.disconnect();
}

/* [SEND MESSAGE] */
void Chat::send(const string& data) {
    if(!chatting){
        return;
    }
        debug("Sending: " << data);
        bt.sendString(data);
        if(data.find(BTPHONE_QUIT)!=string::npos) {
                chatOff();
        }
        if(data.find(BTPHONE_DISCONNECT)!=string::npos) {
                terminate();
        }
}

/* [RECEIVED MESSAGE] */
void Chat::receive(string data){
        debug("Received: " << data);
        if(data.find(BTPHONE_RING)!=string::npos) {
            debug("Connection upcoming!");
                led.startBlink(kLedOrange);
                chatting = true;
                chatCV.broadcast();
                ringBell();
                return;
        }
        if(data.find(BTPHONE_QUIT)!=string::npos) {
            debug("Request to quit");
                chatOff();
                return;
        }
        if(data.find(BTPHONE_DISCONNECT)!=string::npos) {
            debug("Request to terminate");
                terminate();
                return;
        }
        cout << data << endl;
}

// Play the bell audio
void Chat::ringBell() {
        ADPCMSound bell_sound;
        static const unsigned int undersamplingFactor = 4;
        bell_sound.init(bell_bin,bell_bin_len, undersamplingFactor);
        ADPCMPlayer& ADPCMPlayer = ADPCMPlayer::instance();
        ADPCMPlayer.init();
        ADPCMPlayer.play(bell_sound);
        ADPCMPlayer.close();
}
