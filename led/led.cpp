#include "led.h"

//Global functions & variables BEGIN

typedef Gpio<GPIOD_BASE,kLedRed> led_red;
typedef Gpio<GPIOD_BASE,kLedBlue> led_blue;
typedef Gpio<GPIOD_BASE,kLedGreen> led_green;
typedef Gpio<GPIOD_BASE,kLedOrange> led_orange;
bool blinking_red = false;
bool blinking_blue = false;
bool blinking_green = false;
bool blinking_orange = false;
bool animating = false;

void blinkLedWithColor(void *argv) {
    int color = (int)argv;
    float delayS = 0.5;
    switch (color) {
        case kLedBlue:
   		while(blinking_blue) {
		        led_blue::high();
        		usleep(delayS*1000000);
		        led_blue::low();
        		usleep(delayS*1000000);
		}
            break;
        case kLedGreen:
   		while(blinking_green) {
		        led_green::high();
        		usleep(delayS*1000000);
		        led_green::low();
        		usleep(delayS*1000000);
		}
            break;
        case kLedRed:
   		while(blinking_red) {
		        led_red::high();
        		usleep(delayS*1000000);
		        led_red::low();
        		usleep(delayS*1000000);
		}
            break;
        case kLedOrange:
   		while(blinking_orange) {
		        led_orange::high();
        		usleep(delayS*1000000);
		        led_orange::low();
        		usleep(delayS*1000000);
		}
            break;
    }
}

void playAnimation(void *argv) {
    int animation = (int)argv;
    switch (animation) {
        case kLedAnimationWait:
   		while(animating) {
		        led_orange::low();
		        led_red::high();
			delayMs(125);
		        led_red::low();
		        led_blue::high();
			delayMs(125);
		        led_blue::low();
		        led_green::high();
			delayMs(125);
		        led_green::low();
		        led_orange::high();
			delayMs(125);
		}
            break;
        case kLedAnimationAllBlink:
   		while(animating) {
		        led_orange::high();
		        led_green::high();
		        led_blue::high();
		        led_red::high();
			delayMs(125);
		        led_red::low();
		        led_green::low();
		        led_blue::low();
		        led_orange::low();
			delayMs(125);
		}
            break;
    }
}
//Global functions & variables END

Led::Led() {
    led_red::mode(Mode::OUTPUT);
    led_green::mode(Mode::OUTPUT);
    led_blue::mode(Mode::OUTPUT);
    led_orange::mode(Mode::OUTPUT);
}

Led::~Led() { }

Led& Led::instance()
{
        static Led singleton;
        return singleton;
}

void Led::turnOn(int color)
{
    stopBlink(color);
    switch (color) {
        case kLedBlue:
            led_blue::high();
            break;
        case kLedGreen:
            led_green::high();
            break;
        case kLedRed:
            led_red::high();
            break;
        case kLedOrange:
            led_orange::high();
            break;
    }
}

void Led::turnOff(int color)
{
    stopBlink(color);
    switch (color) {
        case kLedBlue:
            led_blue::low();
            break;
        case kLedGreen:
            led_green::low();
            break;
        case kLedRed:
            led_red::low();
            break;
        case kLedOrange:
            led_orange::low();
            break;
    }
}

void Led::startBlink(int color)
{
    stopAnimation();
    switch (color) {
        case kLedRed:
            setBlinkingBool(color,true);
            thread_red = Thread::create(blinkLedWithColor,2048,1,(void*)color,Thread::JOINABLE);
            break;
        case kLedGreen:
            setBlinkingBool(color,true);
            thread_green = Thread::create(blinkLedWithColor,2048,1,(void*)color,Thread::JOINABLE);
            break;
        case kLedBlue:
            setBlinkingBool(color,true);
            thread_blue = Thread::create(blinkLedWithColor,2048,1,(void*)color,Thread::JOINABLE);
            break;
        case kLedOrange:
            setBlinkingBool(color,true);
            thread_orange = Thread::create(blinkLedWithColor,2048,1,(void*)color,Thread::JOINABLE);
            break;
    }
}

void Led::stopBlink(int color)
{
    stopAnimation();
    switch (color) {
        case kLedRed:
            if(blinking_red) {
            	setBlinkingBool(color,false);
                thread_red->join();
            }
            break;
        case kLedGreen:
            if(blinking_green) {
            	setBlinkingBool(color,false);
                thread_green->join();
            }
            break;
        case kLedBlue:
            if(blinking_blue) {
            	setBlinkingBool(color,false);
                thread_blue->join();
            }
            break;
        case kLedOrange:
            if(blinking_orange) {
            	setBlinkingBool(color,false);
                thread_orange->join();
            }
            break;
    }
}

void Led::startAnimation(int animation)
{
    stopBlink(kLedRed);
    stopBlink(kLedBlue);
    stopBlink(kLedGreen);
    stopBlink(kLedOrange);
    stopAnimation();
    {
	Lock<Mutex> lock(mutex_animation);
	animating = true;
    }
    thread_animation = Thread::create(playAnimation,2048,1,(void*)animation,Thread::JOINABLE);
}

void Led::stopAnimation()
{
    if(animating) {
    	{
		Lock<Mutex> lock(mutex_animation);
		animating = false;
    	}
    	thread_animation->join();
    	turnOff(kLedRed);
    	turnOff(kLedGreen);
    	turnOff(kLedBlue);
    	turnOff(kLedOrange);
    }
}

void Led::setBlinkingBool(int color, bool value)
{
    switch (color) {
        case kLedRed:
        	{
	        	Lock<Mutex> lock(mutex_red);
    	    		blinking_red = value;
        	}
            break;
        case kLedGreen:
        	{
	        	Lock<Mutex> lock(mutex_green);
    	    		blinking_green = value;
        	}
            break;
        case kLedBlue:
        	{
	        	Lock<Mutex> lock(mutex_blue);
    	    		blinking_blue = value;
        	}
            break;
        case kLedOrange:
        	{
	        	Lock<Mutex> lock(mutex_orange);
    	    		blinking_orange = value;
        	}
            break;
    }
}
