
#include "button.h"
#include <miosix.h>
#include <miosix/kernel/scheduler/scheduler.h>

using namespace miosix;

//Declaration of a button in Miosix, Port A0, we call it "button"
typedef Gpio<GPIOA_BASE,0> button;

//Global variable: the waiting thread
static Thread *waiting=0;

/** This is the interrupt method that saves the context and calls
*   the implementation, then it restores the context */
void __attribute__((naked)) EXTI0_IRQHandler()
{
    saveContext();
    //a naked function can only support assembly
    //code, we call another function written in C++
    asm volatile("bl _Z16EXTI0HandlerImplv");
    restoreContext();
}

void __attribute__((used)) EXTI0HandlerImpl()
{
    EXTI->PR=EXTI_PR_PR0;

    if(waiting==0) return;
    //Wakes up the waiting thread
    waiting->IRQwakeup();
    if(waiting->IRQgetPriority() > Thread::IRQgetCurrentThread()->IRQgetPriority())
		  Scheduler::IRQfindNextThread();
    //Set waiting to 0 to exit the loop
    waiting=0;
}

void configureButtonInterrupt()
{
    //Set the mode to ground, in way that it goes at Vdd when pressed
    button::mode(Mode::INPUT_PULL_DOWN);
    //EXTI is a peripheral to handle external interrupts
    EXTI->IMR |= EXTI_IMR_MR0;
    EXTI->RTSR |= EXTI_RTSR_TR0;
    //Configure interrupt controller:
    NVIC_EnableIRQ(EXTI0_IRQn);
    NVIC_SetPriority(EXTI0_IRQn,15); //Low priority
}

void waitForButton()
{
    //Disable interrupts for checking, when exiting the scope, they are re-enabled
    FastInterruptDisableLock dLock;
    //Return a pointer to the current thread
    waiting=Thread::IRQgetCurrentThread();
    //waiting is current thread, cannot be null
    while(waiting)
    {
        Thread::IRQwait();
        //Enable back interrupt until the end of the scope (i.e. the next } )
        FastInterruptEnableLock eLock(dLock);
        //Preemption works as an interrupt: for this reason we have to enable them
        //This raises the interrupt handled in EXTI0_IRQHandler
        Thread::yield();
    }
}
